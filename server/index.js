import express from 'express'
import webpack from 'webpack'
import path from 'path'
import config from '../webpack.config'
import open from 'open'
import socket from 'socket.io'
import { Server } from 'http'

const port = 3000
const app = express()
const server = Server(app)
const compiler = webpack(config)
const io = socket(server)

const sessions = []
const users = []


app.use(require('webpack-dev-middleware')(compiler))

app.use(require('webpack-hot-middleware')(compiler))

app.use('*', function(req, res) {  
 res.sendFile(path.join(__dirname, '../src/index.html'))
})

io.on('connection', function(socket) {

  sessions.push(socket)

  socket.on('chat', function(message) {
    socket.broadcast.emit('chat', message)
  })  

  socket.on('users', data => {
    for (let i = 0; i < sessions.length; i++) {
      if (sessions[i] === socket) {
        users.push({sessionId:socket.id, name:data.name})
      } 
    }

    sessions.forEach(connectedSocket => {
      connectedSocket.emit('users', users)
    });
  });

  socket.on('disconnect', () => {

    const index = sessions.indexOf(socket)
    sessions.splice(index, 1)

    for (let i = 0; i < users.length; i++) {
      if (users[i].sessionId == socket.id) {
        users.splice(i, 1)
      }
    }

    sessions.forEach(connectedSocket => {
      connectedSocket.emit('users', users)
    })

  })

})

server.listen(port, function(err) {
  if (err) { 
    console.log('error', err)
   }
})