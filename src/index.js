import 'babel-polyfill'
import React from 'react'
import ReactDOM from 'react-dom'
import { AppContainer } from 'react-hot-loader'
import App from './containers/App'

ReactDOM.render(
  <AppContainer>
    <div className='app'>
      <App/>
    </div>
  </AppContainer>,
  document.getElementById('root')
);