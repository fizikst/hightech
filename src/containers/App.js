import React, { Component } from 'react'
import io from 'socket.io-client'
import { Table } from 'react-bootstrap'

let socket = io('http://localhost:3000');

export default class App extends Component {

  constructor(props){

    super(props);

    this.username = 'Petya.'+new Date().getSeconds()
    
    this.state={
      chat: [],
      users: []
    }

  }


  componentWillMount() {

    let name = prompt('Введите имя:')

    if (name == null || name == '') { 
      name = this.username
    } else {
      this.username = name
    }

    socket.emit('users',{name:name})

  }

  componentDidMount() {

    document.onkeydown = (e) => {
      if (e.keyCode == 27) { // escape
        this.clearMessage()
        return false
      }
    }

    socket.on('chat', data => {
      this.setState({chat:[...this.state.chat,data]})
    })

    socket.on('users', data => {
      this.setState({users:data})
    })

  }

  usrThreads = () => {
    return this.state.users.map((user,index)=>{
      return <div key={index}>
                <span>{user.name}</span>
             </div>
    })
  }

  msgThreads = () => {
    return this.state.chat.map((msg,index)=>{
      return <div key={index}>
                <span>{msg.message}</span>
             </div>
    })
  }

  sendMessage = () => {
    let time = new Date()
    let hh = time.getHours() 
    let mm = time.getMinutes() 
    let ss = time.getSeconds() 
    let msg = `${hh}:${mm}:${ss} - ${this.username} - ${this.textInput.value}`

    this.setState({chat:[...this.state.chat, {message:msg}]})
    socket.emit('chat',{message:msg})
    this.clearMessage()
  }

  handleKeyPress = (ev) => {
    if (ev.which === 13) {
      this.sendMessage()
    }
  }

  clearMessage = () => {
    this.textInput.value = ''
  }

  render() {

    return (
      <div className='container'>
        <Table striped bordered condensed hover>
          <thead>
            <tr>
              <th>Пользователи</th>
              <th>Сообщения</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>{this.usrThreads()}</td>
              <td>{this.msgThreads()}</td>
            </tr>
            <tr>
              <td>
              </td>
              <td>
                <div className='input-group'>
                  <input type='text' className='form-control' onKeyPress={this.handleKeyPress} ref={(input) => { this.textInput = input; }}/>
                  <span className='input-group-btn'>
                    <button className='btn btn-default' type='button' onClick={this.sendMessage} >Отправить</button>
                    <button className='btn btn-default' type='button' onClick={this.clearMessage} >Очистить или ESC</button>
                  </span>
                </div>
              </td>
            </tr>
          </tbody>
        </Table>
      </div>
    )
  }
}